
import java.util.*;

public class OX {
	static String XO[][] = {{" "," "," "},
							{" "," "," "},
							{" "," "," "}};
	
	static String turn = "X";
	static int R, C;
	static int Round = 0;
	
	public static void main(String[] args) {

		printStart();
		/// Start Game///
		for (;;) {
			printBoard();

			/// Choose Row or Column///
			inputPosition();

			/// Check Win Check Draw ///
			if( checkWin() ) {
				break ;
			}
			else if (checkDraw()) {
				break ;
			}
			
			// Swap Turn
			switchTurn();
		}
	}

	public static void printBoard() {
		System.out.println(" " + " 1 " + "2" + " 3 ");
		for (int i = 0; i < 3; i++) {
			System.out.print(i + 1);
			for (int j = 0; j < 3; j++) {
				System.out.print("|" + XO[i][j]);
			}
			System.out.print("|");
			System.out.println();
		}
	}

	public static void printStart() {
		System.out.println("Start Game OX");

	}

	public static void inputPosition() {
		Scanner kb = new Scanner(System.in);

		for (;;) {
			System.out.println("Turn" + " " + turn);
			System.out.print("Plz Choose position(R,C) : ");
			try {
				String a = kb.next();
				String b = kb.next();
				R = Integer.parseInt(a);
				C = Integer.parseInt(b);

				if (R > 3 || R < 0 || R == 0 || C > 3 || C < 0 || C == 0) {
					System.out.println("Row and Column must be number 1-3");
					printBoard();
					continue;
				}
				R = R - 1;
				C = C - 1;

				if (XO[R][C] != " ") {
					System.out.println("Row " + (R + 1) + " and Column " + (C + 1) + " can't choose again");
					printBoard();
					continue;
					
				}
				Round++;
				XO[R][C] = turn;
				break;

			} catch (Exception a) {
				System.out.println("Row and Column must be number");
				printBoard();
				continue;
			}
		}
	}
	public static boolean checkWin() {
		boolean chk = false;
		// ���ǹ͹
		for (int i = 0; i < XO.length; i++) {
			if (XO[i][0] == turn && XO[i][1] == turn && XO[i][2] == turn) {
				chk = true;
			}
		}
		// ���ǵ��
		for (int i = 0; i < XO.length; i++) {
			if (XO[0][i] == turn && XO[1][i] == turn && XO[2][i] == turn) {
				chk = true;
			}
		}
		// �����§
		if (XO[0][0] == turn && XO[1][1] == turn && XO[2][2] == turn) {
			chk = true;
		}
		if (XO[0][2] == turn && XO[1][1] == turn && XO[2][0] == turn) {
			chk = true;
		}
		if (chk == true) {
			printBoard();
			System.out.print(turn + " WIN");
			return true ;
		}
		return false ;
	}
	public static boolean checkDraw() {
		if (Round == 9) {
			printBoard();
			System.out.print("DRAW");
			return true ;
		}
		return false ;
	}
	public static void switchTurn() {
		if (turn == "X") {
			turn = "O";
		} else {
			turn = "X";
		}
	}
}